package com.thumbtack.emomonov.errors;

public class AuctionServiceException extends Exception {
    public AuctionServiceException() {
        super();
    }

    public AuctionServiceException(String message) {
        super(message);
    }

    public AuctionServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuctionServiceException(Throwable cause) {
        super(cause);
    }
}
