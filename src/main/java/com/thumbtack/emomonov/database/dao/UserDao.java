package com.thumbtack.emomonov.database.dao;

import com.thumbtack.emomonov.errors.AuctionServiceException;
import com.thumbtack.emomonov.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends CrudRepository<User, Integer> {
    User findUserByEmail(String email) throws AuctionServiceException;

}
