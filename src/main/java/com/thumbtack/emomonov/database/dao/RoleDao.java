package com.thumbtack.emomonov.database.dao;

import com.thumbtack.emomonov.model.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleDao extends CrudRepository<Role, Integer> {
}
