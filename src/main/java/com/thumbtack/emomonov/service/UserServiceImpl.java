package com.thumbtack.emomonov.service;

import com.thumbtack.emomonov.controller.dto.UserDto;
import com.thumbtack.emomonov.database.dao.RoleDao;
import com.thumbtack.emomonov.database.dao.UserDao;
import com.thumbtack.emomonov.errors.AuctionServiceException;
import com.thumbtack.emomonov.model.Role;
import com.thumbtack.emomonov.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {
    private static Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;
    private User toUser(UserDto userDto){
        Role role = new Role();
        role.setName("USER");
        Set<Role> set = new HashSet<>();
        set.add(role);
        return new User(userDto.getEmail(), set, userDto.getPassword(), userDto.getFirstname(), userDto.getLastname());
    }
    private UserDto toUserDto(User user){
        return new UserDto(user.getEmail(), user.getPassword(), user.getFirstname(), user.getLastname());
    }

    public UserDto addUser(UserDto userDto) throws AuctionServiceException {
        try {

            User user = toUser(userDto);
            LOGGER.debug(user.toString());
            userDao.save(user);
            LOGGER.debug(user.toString());
            return toUserDto(user);
        } catch (RuntimeException e) {
            throw new AuctionServiceException(e);
        }
    }

    @Override
    public List<UserDto> getAllUsers() throws AuctionServiceException {
        try {

            List<UserDto> list = new ArrayList<>();
            for (User user : userDao.findAll()) {
                list.add(toUserDto(user));
            }
            return list;
        } catch (RuntimeException e) {
            throw new AuctionServiceException(e);
        }
    }

    @PostConstruct
    public void saveRoles() throws AuctionServiceException {
        try {
            Role adminRole = new Role();
            adminRole.setName("ADMIN");
            roleDao.save(adminRole);
            Role userRole = new Role();
            userRole.setName("USER");
            roleDao.save(userRole);

            Set<Role> roles = new HashSet<>();
            roles.add(userRole);
            User user = new User("test@mail.ru", roles, "1234", "FirstName", "LastName");
            userDao.save(user);
        } catch (RuntimeException e) {
            throw new AuctionServiceException(e);
        }
    }
}
