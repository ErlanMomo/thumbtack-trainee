package com.thumbtack.emomonov.service;

import com.thumbtack.emomonov.controller.dto.UserDto;
import com.thumbtack.emomonov.errors.AuctionServiceException;

import java.util.List;


public interface UserService {
    UserDto addUser(UserDto userDto) throws AuctionServiceException;

    List<UserDto> getAllUsers() throws AuctionServiceException;

}
