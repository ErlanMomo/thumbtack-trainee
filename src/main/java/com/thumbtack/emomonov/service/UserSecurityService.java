package com.thumbtack.emomonov.service;

import com.thumbtack.emomonov.database.dao.UserDao;
import com.thumbtack.emomonov.model.Role;
import com.thumbtack.emomonov.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class UserSecurityService implements UserDetailsService {
    @Autowired
    private UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDetails loadedUser;
        System.err.println("UserSecurityService: BBBBBBBBBBBBBBBBBBBBBBBBBBB");
        try {
            User userByEmail = userDao.findUserByEmail(username);
            loadedUser = new org.springframework.security.core.userdetails.User(
                    userByEmail.getEmail(),
                    userByEmail.getPassword(),
                    getAuthority(userByEmail)
            );
        } catch (Exception repositoryProblem) {
            throw new InternalAuthenticationServiceException(repositoryProblem.getMessage(), repositoryProblem);
        }
        return loadedUser;
    }

    private Collection<GrantedAuthority> getAuthority(User user) {
        List<GrantedAuthority> collection = new ArrayList<>();
        for (Role role : user.getRoles()) {
            System.err.println("UserSecurityService:I am here, commander!");
            collection.add(new SimpleGrantedAuthority(role.getName()));
        }
        return collection;
    }
}
