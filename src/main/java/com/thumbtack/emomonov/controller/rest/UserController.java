package com.thumbtack.emomonov.controller.rest;

import com.thumbtack.emomonov.controller.dto.UserDto;
import com.thumbtack.emomonov.errors.AuctionServiceException;
import com.thumbtack.emomonov.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private UserService userService;

    @PostMapping("/registration")
    public UserDto registerUser(@RequestBody UserDto userDto) throws AuctionServiceException {
        LOGGER.debug(userDto.toString());
        return userService.addUser(userDto);
    }

    @PostMapping("/login")
    public String login(@RequestBody UserDto userDto) throws AuctionServiceException {
        LOGGER.debug(userDto.toString());
        return "secret";
    }

    @GetMapping
    public List<UserDto> getAllUsers() throws AuctionServiceException {
        return userService.getAllUsers();
    }

}
